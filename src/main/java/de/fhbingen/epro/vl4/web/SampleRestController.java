package de.fhbingen.epro.vl4.web;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @@author Johannes Hiemer.
 */
@RestController
public class SampleRestController {
    
    @RequestMapping("/hello")
    public String hello() {
        return "Hello Spring Boot CLI";
    }
}